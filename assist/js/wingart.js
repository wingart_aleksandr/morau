// $(function(){
// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
// 	ua = navigator.userAgent,

// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

// 	scaleFix = function () {
// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
// 			document.addEventListener("gesturestart", gestureStart, false);
// 		}
// 	};
	
// 	scaleFix();
// });
// var ua=navigator.userAgent.toLocaleLowerCase(),
//  regV = /ipod|ipad|iphone/gi,
//  result = ua.match(regV),
//  userScale="";
// if(!result){
//  userScale=",user-scalable=0"
// }
// document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var slider = $(".flexslider"),
	equal = $(".maxheight");

if(slider.length){
  include("js/jquery.flexslider.js");
}

if (equal.length){
	include("js/matchHeight.js");
}


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(function(){

	if ($("#index_slider").length){
		$('#main_carousel2').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    itemWidth: 220,
		    itemMargin: 0,
		    asNavFor: '#main_slider2'
		});
		 
		$('#main_slider2').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    smoothHeight: true,
		    slideshow: false,
		    sync: "#main_carousel2"
		});
	}
	if ($("#main_slider").length){
		$('#main_carousel').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    itemWidth: 220,
		    itemMargin: 0,
		    asNavFor: '#main_slider'
		});
		 
		$('#main_slider').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    sync: "#main_carousel"
		});
	}

	$(".show_menu_btn , .close").on("click", function(){
	    $("body").toggleClass("show_menu");
	});

	$(document).on("click", function(event) {
      if ($(event.target).closest("nav,.show_menu_btn").length) return;
      $("body").removeClass("show_menu");
      event.stopPropagation();
    });

	$("body").on("click", "#show_thumb_btn", function(event){
		event.preventDefault();

		$("body").toggleClass("show_thumb");
	});
	 
	
})
//-------------Show_menu responsive-------------
